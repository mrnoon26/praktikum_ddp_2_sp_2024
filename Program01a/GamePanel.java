
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JPanel;
import java.util.Random;

public class GamePanel extends JPanel {
	private int xPos = 100, yPos = 100;
    private Random random;

	public GamePanel() {
		random = new Random();
		addKeyListener(new KeyboardInputs(this));
	}

	public void changeXPos(int value) {
		this.xPos += value;
        repaint();
	}

	public void changeYPos(int value) {
		this.yPos += value;
        repaint();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
        Color color = getRndmColor();
		g.setColor(color);
		g.fillOval(xPos, yPos, 50, 50);
	}

	private Color getRndmColor() {
		int r =random.nextInt(255);
		int g =random.nextInt(255);
		int b =random.nextInt(255);
		return new Color(r,g,b);

	}
}
