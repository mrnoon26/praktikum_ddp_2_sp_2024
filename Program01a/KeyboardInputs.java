import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardInputs implements KeyListener {

	private GamePanel gamePanel;

	public KeyboardInputs(GamePanel gamePanel) {
		this.gamePanel = gamePanel;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
        
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
        
	}

	@Override
	public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        switch (keyCode){
            case KeyEvent.VK_W:
            gamePanel.changeYPos(-8);
            break;
            case KeyEvent.VK_S:
            gamePanel.changeYPos(8);
            break;
            case KeyEvent.VK_A :
            gamePanel.changeXPos(-8);
            break;
            case KeyEvent.VK_D :
            gamePanel.changeXPos(8);
            break;
        }

    }
}
