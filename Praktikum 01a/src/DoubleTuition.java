// object yang namanya sesuai dengan nama file .java nya
public class DoubleTuition{
  /*====================================================
                        main program
   =====================================================
   */ 
  public static void main(String[] args) {
    // deklarasi variabel
      double initialTuition = 10000;
      double tuition = initialTuition ;   // Year 0
      // display hasil hitungan dari method-method dibawah
      System.out.println(message01(doubling(initialTuition, tuition)));
      System.out.printf(message02(doublingTuition(tuition, initialTuition), doubling(initialTuition, tuition)));
    }
    //method pertama
    /*
    method ini mereturn string,nama method ini adalah message01,
    parameter method ini adalah variable year yang bertype integer
    */
    public static String message01(int year){
      return ("Tuition will be doubled in "
      + year + " years");
    }
    //method kedua
    /*
    method ini mereturn string berbentuk format,nama method ini adalah message02,
    parameter method ini adalah variable year yang bertype integer 
    dan tuition yang bertype double
    */
    public static String message02(double tuition, int year){
      return String.format("Tuition will be $%.2f in %1d years",
      tuition , year);
    }
    //method ketiga
    /*
    method ini mereturn year yang bertype integer,nama method ini adalah doubling,
    parameter method ini adalah variable initialTuition yang bertype double 
    dan tuition yang bertype double
    */
    public static int doubling(double initialTuition,double tuition){
      //variabel year ini akan bertambah seiring berjalannya whileloop(True)
      int year = 0;
      while (tuition < (2*initialTuition) ) {
        tuition = tuition * 1.07;
        year++;
      }
      return year;
    }
    //method keempat
    /*
    method ini mereturn tuition yang bertype double,nama method ini adalah doublingTuition,
    parameter method ini adalah variable initialTuition yang bertype double 
    dan tuition yang bertype double
    */
    public static double doublingTuition(double tuition, double initialTuition){
      //variabel year ini akan bertambah seiring berjalannya whileloop(True)
      int year = 0;
      while (tuition < (2*initialTuition) ) {
        tuition = tuition * 1.07;
        year++;
      }
      return tuition;
    }
}
