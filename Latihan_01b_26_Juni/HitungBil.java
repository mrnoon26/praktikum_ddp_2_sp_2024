import java.util.Scanner;

public class HitungBil{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Tuliskan bilangan bulat, dan akhiri dengan 0: ");
        int positive = 0;
        int negative =0;
        int sum =0;
        while(true){
            int num = input.nextInt();
            if (num > 0){
                positive++;
            }
            if (num<0){
                negative++;
            }
            sum += num;
            if (num == 0){
                break;
            }
        }
        int totalNum = positive + negative;
        float average = (float) sum/totalNum;
        System.out.println("Bilangan positif ada " + positive);
        System.out.println("Bilangan negatif ada " + negative);
        System.out.println("Total seluruh bilangan adalah " + sum);
        if (!(sum == 0 )){
            System.out.println("Rata-rata seluruh bilangan adalah " + average);
        } else {
            System.out.println("Rata-rata seluruh bilangan adalah " + sum);

        }
    }
}