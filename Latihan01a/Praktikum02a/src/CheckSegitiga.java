public class CheckSegitiga {
    public static void main(String[] argv){


        for (int i=0;i<10;i++){


            // random bilangan bulat mulai dari 1-30
            int a = (int)(Math.random()*30)+1;
            int b = (int)(Math.random()*30)+1;
            int c = (int)(Math.random()*30)+1;


            System.out.printf("Sisi %d, %d, %d adalah ",a,b,c);


            if (a==b && b==c){
                System.out.println("Sama kaki");
            }
            else if(a==b){
                System.out.println("Sama sisi");
            }
            else if (a+b < c) {
                System.out.println("Bukan Segitiga");
            }
            else System.out.println("Sembarang");
        }
    }
}
