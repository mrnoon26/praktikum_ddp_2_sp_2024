//Class  File .java
public class Prima{

    //Method penentu angka prime,atau penyaring angka dari 2-250
    public static boolean isPrime(int num) {
        //Check apakah perkalian i dengan i lebih kecil dari num(yang sedang dicek),jika benar masuk loop
        for (int i = 2; i * i <= num; i++) {
            //Check condition,jika angka yang masuk menghasilkan 0 ketika dimodulo dengan i,maka bukan prima,lompat ke return true
            if (num % i == 0) {
                return false;
            }
        }
        //return true ketika tidak memenuhi loop condition(prima)
        return true;
    }
    //Main program
    public static void main(String[] args) {
        //Buat variable kolom untuk mengeprint newline saat angka yang dibaca sudah mencapai 8
        int kolom = 0;
        //looping angka 2-250
        for (int num = 2; num <= 250; num++) {
            //condition jika method isPrime bernilai true,maka dia akan mendisplay angka prima pada terminal bersamaan dengan spasi
            if (isPrime(num)) {
                System.out.print(num + " ");
                //menambah value kolom untuk memastikan jumlahnya
                kolom++;
                //jika kolom mencapai 8 maka print new line
                if (kolom%8 == 0) {
                    System.out.println();
                }
            }
        }
    }
}