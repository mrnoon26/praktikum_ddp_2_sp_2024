import java.util.*; //import semua library yang ada pada java

public class JumlahHari{//Class nama file .java

    //Main program
    public static void main(String[] args) {
        //Buat object Scanner
        Scanner input = new Scanner(System.in);
        //mendisplay pesan yang meminta input dari user
        System.out.print("Enter a month (1-12): ");
        int month = input.nextInt();
        System.out.print("Enter a year: ");
        int year = input.nextInt();
        //memberhentikan input user,karena cukup
        input.close();

        /* Choosing the month between 12 months */
        String bulan = "";
        if (month == 1){
        bulan = "Januari";
        } else if (month == 2){
            bulan = "Februari";
        } else if (month == 3){
            bulan = "Maret";
        } else if (month == 4){
            bulan = "April";
        } else if (month == 5){
            bulan = "Mei";
        } else if (month == 6){
            bulan = "Juni";
        } else if (month == 7){
            bulan = "Juli";
        } else if (month == 8){
            bulan = "Agustus";
        } else if (month == 9){
            bulan = "September";
        } else if (month == 10){
            bulan = "Oktober";
        } else if (month == 11){
            bulan = "November";
        } else if (month == 12){
            bulan = "Desember";
        } else {
            System.out.println("Bulan tidak diketahui");//jika angka yang dimasukkan user adalah selain 1-12
        }
        //if else condition yang berfungsi untuk menentukan apakah tahun yang dimasukkan user merupakan tahun kabisat atau bukan
        int day=0;
        if ((year%400 == 0)){
            day = 29;
        } else if (year%4 == 0){
            day = 29;
        } else {
            day = 28;
        }
        if (month>=1 && month<=12){
            System.out.println( bulan + " " + year + " has " + day + " days");
        }
        
    }
}